-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2023 at 07:45 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `user_management_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `symbol` varchar(255) NOT NULL,
  `symbol_name` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `symbol`, `symbol_name`, `createdAt`, `updatedAt`) VALUES
(1, 'United States Dollar', '$', ' USD', '2023-08-17 06:26:31', '2023-08-17 06:26:31'),
(2, 'Euro', '€', 'EUR', '2023-08-17 06:27:36', '2023-08-17 06:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `image_link` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image_name`, `image_link`, `createdAt`, `updatedAt`) VALUES
(4, '1692354828680_j2.jpeg', 'localhost:3000/uploads/1692354828680_j2.jpeg', '2023-08-18 10:33:48', '2023-08-18 10:33:48'),
(9, '1692355628030_spa4.jpeg', 'localhost:3000/uploads/1692355628030_spa4.jpeg', '2023-08-18 10:47:08', '2023-08-18 10:47:08'),
(10, '1692355637800_spa4.jpeg', 'localhost:3000/uploads/1692355637800_spa4.jpeg', '2023-08-18 10:47:18', '2023-08-18 10:47:18'),
(11, '1692355644532_spa4.jpeg', 'localhost:3000/uploads/1692355644532_spa4.jpeg', '2023-08-18 10:47:24', '2023-08-18 10:47:24'),
(12, '1692355649190_spa4.jpeg', 'localhost:3000/uploads/1692355649190_spa4.jpeg', '2023-08-18 10:47:29', '2023-08-18 10:47:29'),
(13, '1692355654593_spa4.jpeg', 'localhost:3000/uploads/1692355654593_spa4.jpeg', '2023-08-18 10:47:34', '2023-08-18 10:47:34'),
(14, '1692355661064_spa4.jpeg', 'localhost:3000/uploads/1692355661064_spa4.jpeg', '2023-08-18 10:47:41', '2023-08-18 10:47:41'),
(15, '1692355669770_spa4.jpeg', 'localhost:3000/uploads/1692355669770_spa4.jpeg', '2023-08-18 10:47:50', '2023-08-18 10:47:50');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `shopId` int(11) NOT NULL,
  `image_id` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`image_id`)),
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `description`, `shopId`, `image_id`, `createdAt`, `updatedAt`) VALUES
(3, 'sofa', 200, 'It is used sit', 16, '[9]', '2023-08-18 10:47:08', '2023-08-18 10:47:08'),
(4, 'sofa', 250, 'It is used sit', 16, '[10]', '2023-08-18 10:47:18', '2023-08-18 10:47:18'),
(5, 'sofa', 300, 'It is used sit', 16, '[11]', '2023-08-18 10:47:24', '2023-08-18 10:47:24'),
(6, 'sofa', 350, 'It is used sit', 16, '[12]', '2023-08-18 10:47:29', '2023-08-18 10:47:29'),
(7, 'sofa', 450, 'It is used sit', 16, '[13]', '2023-08-18 10:47:35', '2023-08-18 10:47:35'),
(8, 'sofa', 1500, 'It is used sit', 16, '[14]', '2023-08-18 10:47:41', '2023-08-18 10:47:41'),
(9, 'sofa', 1200, 'It is used sit', 16, '[15]', '2023-08-18 10:47:50', '2023-08-18 10:47:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `image_id` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`image_id`)),
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `role_id`, `image_id`, `createdAt`, `updatedAt`) VALUES
(2, 'ronak', 'Patel', 'jainam@mail.co', '$2b$10$5K4lt0UJKv27C31Mk7p5vORopL1CNYg9l.5C/6HmYKCEHRqk/U8wK', 4, '[19]', '2023-08-18 04:24:26', '2023-08-18 04:36:59'),
(4, 'Ankit', 'soni', 'ankit@gmail.com', '$2b$10$uPl5EhmiZKGXi4twH63nCOSGkSDZGVcAHmov2VeXzFw9nsM31G2PK', 1, '[]', '2023-08-18 05:21:26', '2023-08-18 05:21:26'),
(5, 'prem', 'soni', 'prem@gmail.com', '$2b$10$KqQgJBMD7.WPKjWBXjEsxeNGjXAsY4.qF/PIw8ZPrqNr7BxL9ne0a', 5, '[]', '2023-08-18 05:24:51', '2023-08-18 05:24:51'),
(6, 'yash', 'soni', 'yash@gmail.com', '$2b$10$nNT2zkVi3q6hgX.MpapaRe.eZP6zSRRVbageRolO9/aPGosGlezLG', 1, '[]', '2023-08-18 05:28:11', '2023-08-18 05:28:11'),
(7, 'janvi', 'soni', 'janvi@mail.com', '$2b$10$HOQg4TxR8xj7hLrJ/LtbQutvGm1USUN3HJPDDzKgKFWVmNWOWYVse', 4, '[]', '2023-08-18 06:01:19', '2023-08-18 06:01:19');

-- --------------------------------------------------------

--
-- Table structure for table `user_ratings`
--

CREATE TABLE `user_ratings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_ratings`
--

INSERT INTO `user_ratings` (`id`, `user_id`, `rating`, `description`, `pro_id`, `createdAt`, `updatedAt`) VALUES
(1, 2, 3, 'rate of product 4', 4, '2023-08-18 12:51:36', '2023-08-18 12:51:36'),
(2, 2, 3, 'rate of product 4', 4, '2023-08-18 12:54:05', '2023-08-18 12:54:05'),
(3, 2, 9, 'rate of product 4', 4, '2023-08-18 12:54:12', '2023-08-18 12:54:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_name`, `createdAt`, `updatedAt`) VALUES
(1, 'admin', '2023-08-16 14:00:37', '2023-08-16 14:00:37'),
(2, 'client', '2023-08-16 14:00:50', '2023-08-16 14:00:50'),
(3, 'staff', '2023-08-16 14:01:00', '2023-08-16 14:01:00'),
(4, 'vendor', '2023-08-17 10:27:19', '2023-08-17 10:27:19'),
(5, 'user', '2023-08-18 09:03:10', '2023-08-18 09:03:10');

-- --------------------------------------------------------

--
-- Table structure for table `vender_shops`
--

CREATE TABLE `vender_shops` (
  `id` int(11) NOT NULL,
  `shop_name` varchar(255) NOT NULL,
  `owner_name` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) NOT NULL,
  `shop_no` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vender_shops`
--

INSERT INTO `vender_shops` (`id`, `shop_name`, `owner_name`, `address`, `phone_no`, `shop_no`, `createdAt`, `updatedAt`) VALUES
(12, 'Pizza hut', 'ronak', 'NAdiad', '+916352303105', 'A-203', '2023-08-18 05:46:55', '2023-08-18 05:46:55'),
(13, 'MCd', 'ronak', 'nadiad', '+916352303105', 'A-123', '2023-08-18 07:17:11', '2023-08-18 07:17:11'),
(14, 'MCd', 'ronak', 'nadiad', '+916352303105', 'A-123', '2023-08-18 07:17:21', '2023-08-18 07:17:21'),
(15, 'MCd', 'ronak', 'nadiad', '+916352303105', 'A-123', '2023-08-18 07:17:22', '2023-08-18 07:17:22'),
(16, 'MCd', 'ronak', 'nadiad', '+916352303105', 'A-123', '2023-08-18 07:17:23', '2023-08-18 07:17:23'),
(18, 'MCd', 'ronak', 'nadiad', '+916352303105', 'A-123', '2023-08-18 07:17:26', '2023-08-18 07:17:26'),
(19, 'MCd', 'ronak', 'nadiad', '+916352303105', 'A-123', '2023-08-18 07:17:27', '2023-08-18 07:17:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `email_2` (`email`),
  ADD UNIQUE KEY `email_3` (`email`),
  ADD UNIQUE KEY `email_4` (`email`),
  ADD UNIQUE KEY `email_5` (`email`),
  ADD UNIQUE KEY `email_6` (`email`),
  ADD UNIQUE KEY `email_7` (`email`),
  ADD UNIQUE KEY `email_8` (`email`),
  ADD UNIQUE KEY `email_9` (`email`),
  ADD UNIQUE KEY `first_name` (`first_name`),
  ADD UNIQUE KEY `email_10` (`email`),
  ADD UNIQUE KEY `first_name_2` (`first_name`),
  ADD UNIQUE KEY `email_11` (`email`);

--
-- Indexes for table `user_ratings`
--
ALTER TABLE `user_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vender_shops`
--
ALTER TABLE `vender_shops`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_ratings`
--
ALTER TABLE `user_ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vender_shops`
--
ALTER TABLE `vender_shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
